![](http://cdn.bulbagarden.net/upload/f/f0/014Kakuna.png)

kakuna
---

ES6 promise wrapper around superagent. Currently using `native-promise-only`, but it should be removed any time later.

Why
---

There's a plenty of ways to integrate superagent and promises, this is just one of them. It has a better API, by making `.end()` return a promise.

Usage
---
Install it

```bash
npm install --save kakuna

```

Just like superagent, but promesified:

```javascript
var request = require('kakuna')

request
  .post('/api/pet')
  .send({ name: 'Manny', species: 'cat' })
  .set('X-API-Key', 'foobar')
  .set('Accept', 'application/json')
  .end()
  .then((res) => {
  	// do what you want with my res.body
  })

```

License
---

MIT
